var env = {};

// Import variables if present (from env.js)
if(window){  
  Object.assign(env, window.__env);
}

angular.module('starter', [ 'starter.controllers', 'starter.services', 'ui.router', 'ngSanitize', 'ngResource', 'infinite-scroll', 'ui.bootstrap'])

.constant('__env', env)

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


  // Each tab has its own nav history stack:
  .state('home', {
    url: '/home',
    cache: false,
    templateUrl: 'views/home.html',
    controller: 'HomeCtrl'
  })

  // setup an abstract state for the tabs directive
  .state('blog', {
    url: '/blog',
    templateUrl: 'views/blog.html',
    controller: 'BlogCtrl'
  })

  .state('givme5', {
    url: '/givme5',
    templateUrl: 'views/givme5.html',
    controller: 'Givme5Ctrl'
  })

  .state('last5', {
    url: '/last5',
    templateUrl: 'views/last5.html',
    controller: 'Givme5BetterWayToDoItCtrl'
  })

  .state('prevNextBoris', {
    url: '/first',
    templateUrl: 'views/prevNextBoris.html',
    controller: 'PrevNextBorisCtrl'
  })

  .state('random', {
    url: '/random',
    templateUrl: 'views/random.html',
    controller: 'RandomCtrl'
  })

  .state('details', {
    url: '/blog/:postId',
    templateUrl: 'views/details.html',
    controller: 'DetailsCtrl', function($stateParams){
      $stateParams.postId
    }
  })

  .state('infiniteScroll', {
    url: '/infinite',
    templateUrl: 'views/infiniteScroll.html',
    controller: 'InfiniteScrollCtrl'
  })

  .state('update', {
    url: '/post/{postId:int}/update',
    templateUrl: 'views/update.html',
    controller: 'UpdateCtrl'
  })

  .state('prevNext', {
    url: '/post/{postId:int}',
    templateUrl: 'views/PrevNext.html',
    controller: 'PrevNextCtrl'
  })

  .state('blogInfinite', {
    url: '/blogInfinite',
    templateUrl: 'views/blogInfinite.html',
    controller: 'BlogInfiniteScroll'
  })

  .state('carousel', {
    url: '/carousel',
    templateUrl: 'views/carousel.html',
    controller: 'CarouselCtrl'
  })

  .state('postCarousel', {
    url: '/post/carousel',
    templateUrl: 'views/postCarousel.html',
    controller: 'PostCarouselCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});