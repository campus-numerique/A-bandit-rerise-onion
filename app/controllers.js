angular.module('starter.controllers', [])

.controller('HomeCtrl', ['PostService', function($scope, $state, PostService, config) {
  // Exemple d'utilisation du PostService :

  // Un seul post :
  // var post = PostService.get({id: $scope.id});
  // $scope.post = post;

  // Tous les posts :
  // var posts = PostService.query();
  // $scope.posts = posts;
}])

.controller('Givme5Ctrl', function($scope, $state, $http) {

  $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/posts').then(function (response) {
    $scope.posts = response.data;
    console.log(response.data);
  }, function (error) {
    console.log(error);
    $message = "Erreur sur Givme5Ctrl";
    $scope.message=$message;
  })
  $scope.showPost = function(id) {
    console.log(id);
    $scope.test=$scope.posts[id];

    $scope.testId=id;
  }
  // -- If currentID = '2', The next button should display '4'--//
  $scope.previousPost = function() {
    var currentIndex = $scope.dressArray.indexOf(currentID.toString());
    var prev = (currentIndex - 1) < 0 ? $scope.dressArray[0] : $scope.dressArray[currentIndex - 1];
    $state.go('main.rail', prev);
  }
  $scope.nextPost = function() {
    var currentIndex = $scope.dressArray.indexOf(currentID.toString());
    var next = (currentIndex + 1) > $scope.dressArray.length ? $scope.dressArray[dressArray.length - 1] : $scope.dressArray[currentIndex + 1];
    $state.go('main.rail', next);
  }
})

.controller('Givme5BetterWayToDoItCtrl', function($scope, $state, $http, DataLoaderDELETE, DataLoaderPOST, Base64) {

  //////////////////GET 5 LAST POSTS/////////////////////////////////////////////////////////////////
  $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/posts?per_page=5').then(function (response) {

    $scope.posts = response.data;

    //////////////////GET TAGS/////////////////////////////////////////////////////////////////
    $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/tags?per_page=100').then(function (response) {
      $scope.tags = response.data;
    })

  }, function (response) {

    $message = "Erreur sur Givme5BetterWayToDoItCtrl";
    $scope.message=$message;

  })

  var username = __env.username;
  var password = __env.password;
  var base64 = Base64.encode( username + ':' + password );

  //////////////////DELETE A POST////////////////////////////////////////////////////////////////
  $scope.delete = function ($id) {

    $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/' + $id;

    DataLoaderDELETE.getAuth( base64, $url ).then(function(response) {

      $scope.data = response.data;
      console.log(response);
      $state.reload();

    }, function(response) {

      console.log('Error');
      console.log(response);
    });

  }

  //////////////////CREATE A POST/////////////////////////////////////////////////////////////////
  $scope.create = function () {

    $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/';

    console.log($scope.blog.title);
    console.log($scope.blog.content);
    var post =
    {
      'title': $scope.blog.title,
      'content': $scope.blog.content,
      'status': 'publish'
    };

    DataLoaderPOST.getAuth( base64, $url, post ).then(function(response) {
      $state.reload();
    }, function (error) {
      console.log(error);
    })
  }
})

.controller('InfiniteScrollCtrl', function($scope) {
  $scope.images = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  $scope.loadMore = function() {
    var last = $scope.images[$scope.images.length - 1];
    for(var i = 1; i <= 10; i++) {
      $scope.images.push(last + i);
    }
  };
})

.controller('BlogCtrl', function($scope, $state, $http, DataLoaderDELETE, Base64) {

  /////////////////////GET ALL POSTS////////////////////////
  $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts?per_page=100';
  $http.get($url).then(function (response) {
    $scope.posts = response.data;
    console.log($scope.posts);

    $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/tags?per_page=100').then(function (response) {
      $scope.tags = response.data;
      console.log($scope.tags)
    })
    //////////////////DELETE A POST/////////////////////
    $scope.delete = function ($id) {

      ////////////////AUTHENTICATE//////////////////////
      var username = __env.username;
      var password = __env.password;
      var base64 = Base64.encode( username + ':' + password );
      $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/' + $id;

      DataLoaderDELETE.getAuth( base64, $url ).then(function(response) {

        $scope.data = response.data;
        console.log(response);
        $state.reload();

      }, function(response) {

        console.log('Error');
        console.log(response);
      });

    }
  })
})

.controller('BlogInfiniteScroll', function($scope, $state, $http) {
 $scope.posts = [];
 $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts?per_page=5';
 $http.get($url).then(function (response) {

  for(var i = 0; i < 5; i++) {
    $scope.posts.push(response.data[i]);
  }
}, function (error) {
  console.log(error);
})

 $scope.nextPage = function() {
  console.log($scope.posts.length);
  $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts?per_page=5&offset=' + $scope.posts.length;
  $http.get($url).then(function (response) {
    for(var i = 0; i < 5; i++) {
      // if($scope.posts.length <= response.data.length) {
      $scope.posts.push(response.data[i])}
    }, function (error) {
      console.log(error);
      })
    };
  console.log('Error');
})

.controller('RandomCtrl', function($scope, $state, $http) {
  $http.get('http://icone-div.com/wordpress/wp-json/wp/v2/posts').then(function (response) {
    $scope.posts = response.data;
    $scope.random = function() {
      return 0.5 - Math.random();
    }
  }, function errorCallback(response) {
    $message = "Erreur sur RandomCtrl";
    $scope.message=$message;
  })
})


// autre méthode de random
// nbPosts = response.data.length;
// $scope.random = function() {
//   x = Math.floor((Math.random() * nbPosts) + 1);
//   return x;
// }

.controller('AnotherRandomCtrl', ['PostService', function(PostService, $scope){
  var posts = PostService.query();
  $scope.posts = posts;

  $scope.random = function() {
    return 0.5 - Math.random();
  }
}])

.controller('DetailsCtrl', function($scope, $http, $state, $stateParams, Base64, DataLoaderPOST) {


  var username = __env.username;
  var password = __env.password;
  var base64 = Base64.encode( username + ':' + password );
  $scope.showTag = false;

  var $id = $stateParams.postId;
  $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/'+ $id;
  $http.get($url).then(function(response){
    $scope.test = response.data;

    //GET COMMENTS
    $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/comments').then(function(response) {
      $scope.comments = response.data;

    //////////////////GET TAGS/////////////////////////////////////////////////////////////////
    $http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/tags?per_page=100').then(function (response) {
      $scope.tags = response.data;
    })

    $scope.addTag = function () {

      console.log($scope.newTag);
      data = {
        'name': $scope.newTag
      };

      $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/tags/';
      DataLoaderPOST.getAuth(base64, $url, data).then(function(response){

        $url2 = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/' + $id;
        $scope.test.tags.push(response.data.id);
        var data2 = {
          'tags': $scope.test.tags
        };
        DataLoaderPOST.getAuth(base64, $url2, data2).then(function(response){
          console.log(response);
          $state.reload();
        }, function (error) {
          console.log(error)

        });

        console.log(response);

      }, function (error) {
        console.log(error);
        if (error.data.code == 'term_exists') {
          $scope.test.tags.push(error.data.data);
          data3 = {
            'tags' : $scope.test.tags
          };
          $url2 = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/' + $id;
          DataLoaderPOST.getAuth(base64, $url2, data3).then(function(response){
            console.log(response);
            $state.reload();
          }, function (error) {
            console.log(error)

          });
        }
        console.log(error.data.code)
      });
    }

  }, function errorCallback(response) {
    $message = "Erreur sur DetailsCtrl";
    $scope.message=$message;
  })
})
})

.controller('UpdateCtrl', function($scope, $http, $state, $stateParams, Base64, Update, $location) {
  var $id1 = $stateParams.postId;
  $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/'+ $id1;
  $http.get($url).then(function(response){
    $scope.post = response.data;
    $scope.title =response.data.title.rendered;
    $scope.content =response.data.content.rendered;
  }, function errorCallback(response) {
    $message = "Erreur sur UpdateCtrl";
    $scope.message=$message;
  })

  $scope.update = function($id) {
    var username = __env.username;
    var password = __env.password;
    var base64 = Base64.encode( username + ':' + password );
    var data = {
      'title': $scope.title,
      'content': $scope.content
    }

    $url = 'http://blog.adrien-juhem.eu/wp-json/wp/v2/posts/' + $id;
    Update.getAuth(base64, $url, data).then(function(response){

      $scope.data = response.data;
      //$state.reload();
      console.log(response.data.id);
      $location.path("/blog/" + response.data.id);
    }, function errorCallback(response) {
      $message = "Erreur sur getAuthUpdateCtrl";
      $scope.message=$message;
    });
  }
})

.controller('PrevNextBorisCtrl', function($scope, $http, $state, $stateParams) {
	$http.get('http://blog.adrien-juhem.eu/wp-json/wp/v2/posts?per_page=100&filter[orderby]=id&order=asc').then(function (response) {

    $index = 0;
    $scope.first = true;
    $scope.last = false;
    $scope.post = response.data[$index];
    $nbPosts = (response.data.length);
    $scope.previousPost = function() {
      if ($index > 0){
       $index--;
       $scope.last=false;
       $scope.post = response.data[$index];
       if($index==0){
        $scope.first=true;
      }
    }
  };

  $scope.nextPost = function() {
    if ($index < $nbPosts -1){
     $index++;
     if($index == $nbPosts -1){
      $scope.last=true;
    }
    $scope.first = false;
    $scope.post = response.data[$index];
  }
};
}, function errorCallback(response) {
 $message = "Erreur sur PrevNextBorisCtrl";
 $scope.message=$message;
})
})

.controller('CarouselCtrl', function($scope) {
  $scope.myInterval = 3000;
  $scope.slides = [
  {
    image: 'https://68.media.tumblr.com/094f6e39edabad555deb78fa0ba1493e/tumblr_oqbfthOjZO1skk0t3o1_540.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/81c1eed37aaf9b851a5f4b340bd63627/tumblr_o6zy5jKSFM1qzru0ao1_1280.jpg'
  },
  {
    image: 'https://68.media.tumblr.com/af768099b27f314f317d1bf08ff716de/tumblr_oqe4tpCyaX1vlubllo1_540.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/e16dbd92b438bc2970c44745105bff00/tumblr_o7bdwsrtus1rn9ux3o1_540.jpg'
  },
  {
    image: 'https://68.media.tumblr.com/5c518cdafeaf317f5ab382e283caf27d/tumblr_oqbujoasOd1twd8ddo1_500.gif'
  },
  {
    image: 'https://68.media.tumblr.com/6bca7ea17b7f0b64a954f894443836b2/tumblr_ol92afWF5p1rov3pqo1_540.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/c80b261a65ba01888f292b8f6ba5b16d/tumblr_o672iixDwW1qaz1ado1_1280.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/e8f5a381e80a2e671ce0cc6089214d0d/tumblr_o6enshK8y71qz6f9yo4_1280.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/cf7df24efd6cea5a2e4cf1165ebce919/tumblr_oa33z1oc4g1sqf5tdo1_1280.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/7bbf1c187432c63abf27ec4430347856/tumblr_o5ftj5YyAL1twrbr9o1_r1_540.gif'
  },
  {
    image: 'http://68.media.tumblr.com/tumblr_mb8un0s6ju1rz1st5o1_1280.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/ce308c12db3e8ccff785947bbed33fd7/tumblr_o791ufQI5p1rsitbio1_1280.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/e6c32073f6117cf63e175e4e980952f2/tumblr_o4cuh8Xseu1r539hzo1_500.jpg'
  },
  {
    image: 'http://68.media.tumblr.com/b64f9dcd131e1a001848d373abcb0216/tumblr_nqvmmiGFQS1qzgjfco1_1280.gif'
  }
  ];
})

.controller('PostCarouselCtrl', function($scope, $http) {
  $scope.myInterval = 3000;
  $index = 0;

  $url = 'http://icone-div.com/wordpress/wp-json/wp/v2/posts/';
  $http.get($url).then(function(response){
    $scope.postSlides = response.data;
    console.log(response.data);

  }, function errorCallback(response) {
    $message = "Erreur sur PrevNextCtrl";
    $scope.message=$message;
  })

});
